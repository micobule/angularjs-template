module.exports = function(grunt) {
    var NG_APP_ROOT = 'app';
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        // grunt jshint
        jshint: {
            'ng-app': {
                files: {
                    src: [NG_APP_ROOT + '/**/*.js']
                }
            }
        },
        
        // grunt uglify
        uglify: {
            'ng-app': {
                options: {
                    mangle: false,
                    sourceMap: 'public/js/ng.js.map',
                    sourceMappingURL: 'ng.js.map'
                },
                src: [
                    NG_APP_ROOT + '/**/*.js',
                    '!' + NG_APP_ROOT + '/**/*.spec.js'
                ],
                dest: 'public/js/ng.js'
            }
        },
        
        // grunt jade
        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: [
                    {
                        cwd: 'app/',
                        expand: true,
                        ext: ".html",
                        src: '**/*.jade',
                        dest: 'public/'
                    }
                ]
            }
        },
        
        // grunt stylus
        stylus: {
            compile: {
                options: {
                    compress: true
                },
                files: {
                    'public/css/styles.css': 'app/styles/styles.styl',
                }
            }
        },
        
        // grunt watch
        watch: {
            options: {
                livereload: 35730
            },
            'ng-app': {
                files: NG_APP_ROOT + '/**/*.js',
                tasks: ['set-env-development', 'jshint:ng-app', 'uglify:ng-app'],
                options: {
                    spawn: false,
                }
            },
            jade: {
                files: 'app/**/*.jade',
                tasks: ['set-env-development', 'jade'],
                options: {
                    spawn: false,
                }
            },
            styles: {
                files: 'app/**/*.styl',
                tasks: ['set-env-development', 'stylus'],
                options: {
                    spawn: false,
                }
            }
        },

        // grunt bowercopy
        bowercopy: {
            options: {
                runBower: false,
                destPrefix: 'public/bower'
            },
            angular: {
                files: {
                    'angular/angular.min.js': 'angular/angular.min.js',
                    'angular/angular.min.js.map': 'angular/angular.min.js.map',
                    'angular/angular-route/angular-route.min.js': 'angular-route/angular-route.min.js',
                    'angular/angular-route/angular-route.min.js.map': 'angular-route/angular-route.min.js.map',
                    'angular/angular-ui-router/angular-ui-router.min.js': 'angular-ui-router/release/angular-ui-router.min.js'
                }
            },
            'flat-ui': {
                files: {
                    'flat-ui': 'flat-ui/dist'
                }
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bowercopy');
    
    grunt.registerTask('build-app', ['jshint', 'uglify', 'jade', 'stylus']);
        
    grunt.registerTask('set-env-development', function () {
        process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    });
};