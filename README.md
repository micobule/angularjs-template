# AngularJS Template

AngularJS base template for Cynder projects

### Stack
 - **Simple HTTP Server:** [http-server](https://github.com/indexzero/http-server)
 - **Client-side MVC Framework:** [AngularJS](http://angularjs.org/)
 - **Bootstrap Theme:** [Flat-UI](http://designmodo.github.io/Flat-UI/)
 - **Unit Testing:**
 	- [Karma](http://karma-runner.github.io/)
 	- [Jasmine](http://jasmine.github.io/2.3/introduction.html)

### Installation

**NOTE:** You must have Node Package Manager (`npm`) & Bower (`bower`) installed.
**NOTE:** You must have Grunt CLI (task runner) & Karma CLI (test runner). *hint `npm install grunt-cli karma-cli -g`*

1. Run `npm install` on your terminal. *Bower installation is already hooked to the `npm install` command.*
2. Run `grunt build-app` to generate the public folder and all assets.
3. Run `grunt bowercopy` to copy Bower assets from bower_components to public/bower folder.

### Execute the Server Locally

If you have the http-server package installed in your global NPM namespace, use `http-server -p <PORT>` where PORT is the desired port number. Default is 8080 (and I'm using 8000).

If you want to use the http-server module in the local project, use `node node_modules/.bin/http-server -p <PORT>`.

### Unit Testing

Just run `karma start` on your root folder and you should see the sample test suite for HomeController in the terminal.

### Dev Notes
 - While developing the app, run `grunt watch` on a terminal so it would automatically recompile everything (Jade, Stylus, minification, etc.).