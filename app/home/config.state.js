(function() {
	'use strict';

	angular
		.module('app.home')
		.run(appRun);

	// appRun.$inject = ['routerHelper']
    
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }
    
    function getStates() {
        return [
            {
                state: 'home',
                config: {
                    parent: 'layout',
                    views: {
                        'content': {
                            templateUrl: 'home/home.html',
                            controller: 'HomeController as vm'
                        }
                    }
                }
            },
            {
                state: 'home.dashboard',
                config: {
                    url: '/home',
                    views: {
                        'graph': {
                            templateUrl: 'home/graph.html'
                        },
                        'summary': {
                            template: '<h3 class="text-center">div.col-sm-6</h3>'
                        },
                        'notifications': {
                            template: function(params) {
                                return '<h3 class="text-center">div.col-sm-6</h3>';
                            }
                        }
                    }
                }
            }
        ];
    }
})();