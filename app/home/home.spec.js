describe('HomeController', function() {
	var controller = {};

	beforeEach(module('app.home'));
	beforeEach(inject(function($controller) {
		controller = $controller('HomeController', {});
	}));

	it('should have testMe', function() {
		expect(controller.testMe).toBeTruthy(true);
	});
});